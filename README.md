# meta-openssl-caam

#### Description
Enhance cryptodev and its engine in OpenSSL by CAAM's public key cryptography operations

#### Software and Hardware platform
Linux kernel: lf-5.10.y

cryptodev: 1.12

OpenSSL: 1.1.1l

HW platform: i.MX6UL, i.MX7D/S, i.MX8M/8M Mini/8M Nano/8M Plus, i.MX8/8X.

#### Installation

1.  copy meta-openssl-caam to folder <Yocto dir>/sources/
2.  Run DISTRO=fsl-imx-xwayland MACHINE=imx8mmevk source imx-setup-release.sh -b build-imx8mmevk and add BBLAYERS += " ${BSPDIR}/sources/meta-openssl-caam " into <Yocto dir>/build-imx8mmevk/conf/bblayers.conf
3.  bitbake imx-image-multimedia